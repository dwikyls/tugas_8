<?php

return [

    /*
     * Example Menu
     */
    'Sikampus' => [
        'menu' => [
            'Student' => ['url' => 'student', 'data' => ['icon' => 'circle outline']],
            'Teacher' => ['url' => 'teacher', 'data' => ['icon' => 'circle outline']],
            'Course' => ['url' => 'course', 'data' => ['icon' => 'circle outline']],
        ],
    ],
];
