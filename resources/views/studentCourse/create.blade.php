@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Tambah Courses" />

    {!! form()->open(route('student.store')) !!}
        @include('studentCourse._form')
    {!! form()->close() !!}

@stop
