@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Courses">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('course.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! Suitable::source($courses)->search()->columns([
            \Laravolt\Suitable\Columns\Numbering::make('No'),
            \Laravolt\Suitable\Columns\Text::make('nama', 'Nama')->sortable('nama'),
            \Laravolt\Suitable\Columns\Text::make('kelas', 'Kelas'),
            \Laravolt\Suitable\Columns\Text::make('sks', 'Sks'),
            \Laravolt\Suitable\Columns\RestfulButton::make('course', 'Aksi')
        ])->render() 
    !!}

@stop
