@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Tambah Course" />

    {!! form()->open(route('course.store')) !!}
        @include('course._form')
    {!! form()->close() !!}

@stop
