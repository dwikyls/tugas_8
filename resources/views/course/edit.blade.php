@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Edit Course" />

    {!! form()->bind($course)->put(route('course.update', $course->id)) !!}
        @include('course._form')
    {!! form()->close() !!}

@stop
