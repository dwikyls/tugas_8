@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Edit Student" />

    {!! form()->bind($student)->put(route('student.update', $student->id)) !!}
        @include('student._form')
    {!! form()->close() !!}

@stop
