{!! form()->text('nama')->placeholder('Nama Lengkap')->label('Nama') !!}
{!! 
    form()->select('jenis_kelamin')
    ->placeholder('Pilih')
    ->appendOption('L', 'Laki-laki')
    ->appendOption('P', 'Perempuan')
    ->label('Jenis Kelamin');
!!}
{!! form()->text('tempat_lahir')->placeholder('Tempat Lahir')->label('Tempat Lahir') !!}
{!! form()->date('tanggal_lahir')->label('Tanggal Lahir') !!}
{!! form()->submit(); !!}