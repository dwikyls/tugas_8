@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Student">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('student.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! Suitable::source($students)->search()->columns([
            \Laravolt\Suitable\Columns\Numbering::make('No'),
            \Laravolt\Suitable\Columns\Text::make('nama', 'Nama')->sortable('nama'),
            \Laravolt\Suitable\Columns\RestfulButton::make('student', 'Aksi')
        ])->render() 
    !!}

@stop
