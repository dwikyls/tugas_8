@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Tambah Student" />

    {!! form()->open(route('student.store')) !!}
        @include('student._form')
    {!! form()->close() !!}

@stop
