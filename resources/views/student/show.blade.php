@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Detail Student" />

    <div class="ui two column doubling stackable grid container">
        <div class="column">
            <div class="ui card">
                <div class="image">
                    <img src="/images/default.png" alt="">
                </div>
                <div class="content">
                    <a class="header">{{ $student->nama }}</a>
                    <div class="meta">
                        <span class="date">Joined in {{ $student->created_at }}</span>
                    </div>
                    <div class="description">
                        Kristy is an art director living in New York.
                    </div>
                </div>
                <div class="extra content">
                    <a>
                        <i class="user icon"></i>
                        22 Friends
                    </a>
                </div>
            </div>
        </div>
        <div class="column">
            {!! Suitable::source($studentCourses)->search()->columns([
                    \Laravolt\Suitable\Columns\Numbering::make('No'),
                    \Laravolt\Suitable\Columns\Text::make('nama', 'Nama')->sortable('nama'),
                    \Laravolt\Suitable\Columns\Text::make('kelas', 'Kelas'),
                    \Laravolt\Suitable\Columns\Text::make('sks', 'Sks'),
                    \Laravolt\Suitable\Columns\RestfulButton::make('course', 'Aksi')->except('view', 'edit')
                ])->render() 
            !!}

            {!! form()->post(route('studentCourse.store')) !!}
                {!! form()->hidden('student_id', $student->id); !!}
                {!! form()->dropdown('course_id', $courses)->placeholder('...')->label('Pilih Mata Kuliah') !!}
                {!! form()->submit(); !!}
            {!! form()->close() !!}
        </div>
    </div>

@stop
