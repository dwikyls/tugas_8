@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Tambah Riwayat Pendidikan" />

    {!! form()->open(route('background.store')) !!}
        @include('background._form')
    {!! form()->close() !!}

@stop
