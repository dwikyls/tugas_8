@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Detail Teacher" />

    <div class="ui two column doubling stackable grid container">
        <div class="column">
            <div class="ui card">
                <div class="image">
                    <img src="/images/default.png" alt="">
                </div>
                <div class="content">
                    <a class="header">{{ $teacher->nama }}</a>
                    <div class="meta">
                        <span class="date">Joined in {{ $teacher->created_at }}</span>
                    </div>
                    <div class="description">
                        Kristy is an art director living in New York.
                    </div>
                </div>
                <div class="extra content">
                    <a>
                        <i class="user icon"></i>
                        22 Friends
                    </a>
                </div>
            </div>
        </div>
        <div class="column">
            {!! Suitable::source($teacherCourses)->search()->columns([
                    \Laravolt\Suitable\Columns\Numbering::make('No'),
                    \Laravolt\Suitable\Columns\Text::make('nama', 'Nama')->sortable('nama'),
                    \Laravolt\Suitable\Columns\Text::make('kelas', 'Kelas'),
                    \Laravolt\Suitable\Columns\Text::make('sks', 'Sks'),
                    \Laravolt\Suitable\Columns\RestfulButton::make('course', 'Aksi')->except('view', 'edit')
                ])->render() 
            !!}

            {!! form()->post(route('teacherCourse.store')) !!}
                {!! form()->hidden('teacher_id', $teacher->id); !!}
                {!! form()->dropdown('course_id', $courses)->placeholder('...')->label('Pilih Mata Kuliah') !!}
                {!! form()->submit(); !!}
            {!! form()->close() !!}

            {!! Suitable::source($backgrounds)->search()->columns([
                    \Laravolt\Suitable\Columns\Numbering::make('No'),
                    \Laravolt\Suitable\Columns\Text::make('strata', 'Strata')->sortable('nama'),
                    \Laravolt\Suitable\Columns\Text::make('jurusan', 'Jurusan'),
                    \Laravolt\Suitable\Columns\Text::make('sekolah', 'Sekolah'),
                    \Laravolt\Suitable\Columns\Text::make('tahun_mulai', 'Mulai'),
                    \Laravolt\Suitable\Columns\Text::make('tahun_selesai', 'Selesai'),
                    \Laravolt\Suitable\Columns\RestfulButton::make('course', 'Aksi')->except('view', 'edit')
                ])->render()
            !!}

            {!! form()->post(route('background.store')) !!}
                {!! form()->hidden('teacher_id', $teacher->id); !!}
                {!! form()->text('strata')->label('Strata') !!}
                {!! form()->text('jurusan')->label('Jurusan') !!}
                {!! form()->text('sekolah')->label('Sekolah') !!}
                {!! form()->text('tahun_mulai')->label('Tahun Mulai') !!}
                {!! form()->text('tahun_selesai')->label('Tahun Selesai') !!}
                {!! form()->submit(); !!}
            {!! form()->close() !!}
        </div>
    </div>

@stop
