@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Tambah Teacher" />

    {!! form()->open(route('teacher.store')) !!}
        @include('teacher._form')
    {!! form()->close() !!}

@stop
