@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Edit Teacher" />

    {!! form()->bind($teacher)->put(route('teacher.update', $teacher->id)) !!}
        @include('teacher._form')
    {!! form()->close() !!}

@stop
