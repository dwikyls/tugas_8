@extends('laravolt::layouts.app')

@section('content')

    <x-titlebar title="Teacher">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('teacher.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! Suitable::source($teachers)->search()->columns([
            \Laravolt\Suitable\Columns\Numbering::make('No'),
            \Laravolt\Suitable\Columns\Text::make('nama', 'Nama')->sortable('nama'),
            \Laravolt\Suitable\Columns\RestfulButton::make('teacher', 'Aksi')
        ])->render() 
    !!}

@stop
