<?php

use App\Http\Controllers\BackgroundController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\Home;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentCourseController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\TeacherCourseController;

Route::get('/', Home::class)->name('home');
Route::get('/dashboard', Dashboard::class)->name('dashboard')->middleware('auth');
Route::resource('student', StudentController::class);
Route::resource('teacher', TeacherController::class);
Route::resource('course', CourseController::class);
Route::resource('background', BackgroundController::class);
Route::resource('studentCourse', StudentCourseController::class);
Route::resource('teacherCourse', TeacherCourseController::class);
