<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Teacher extends Model
{
    use HasFactory;
    use AutoSearch;
    use AutoSort;

    protected $searchableColumns = ['nama'];
    protected $fillable = ['nama', 'gelar'];

    public function backgrounds()
    {
        return $this->hasMany(Background::class);
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_teacher', 'teacher_id', 'course_id');
    }
}
