<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Background extends Model
{
    use HasFactory;

    protected $fillable = ['teacher_id', 'strata', 'jurusan', 'sekolah', 'tahun_mulai', 'tahun_selesai'];

    public function teachers()
    {
        return $this->belongsTo(Teacher::class);
    }
}
