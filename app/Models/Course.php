<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Course extends Model
{
    use HasFactory;
    use AutoSearch;
    use AutoSort;

    protected $fillable = ['nama', 'sks', 'kelas'];

    public function students()
    {
        return $this->belongsToMany(Student::class, 'course_student', 'student_id', 'course_id');
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class, 'course_teacher');
    }
}
